
代码生成器：<br>
该项目为代码生成器 基于Apache Velocity的 Java模板引擎<br>
特色：生成代码模板可自定义，依据自己的项目代码风格，自定义代码模板<br>
自定义代码步骤<br>
（1）添加模板加载地址resources/static/template.json<br>
（2）具体模板存放地址src/main/resources/<br>

运行：<br>
com.mmk.BaseApplication.java或生成jar包：run Maven intall<br>

访问路径：http://localhost:10001/codeUI/index.html<br>

不可用ip 访问<br>
登录界面输入的是作者<br>
![项目页面](https://gitee.com/uploads/images/2019/0426/114737_05444f9a_921626.jpeg)
![项目页面](https://gitee.com/uploads/images/2019/0426/114737_1e5c25ed_921626.jpeg)
![项目页面](https://gitee.com/uploads/images/2019/0426/114737_c03dfd27_921626.jpeg)

 

com.mmk.code.common.PropertyNameTools.java 设置表初始加载的时候数据库字段类型对应的实体字段类型<br>

 
模拟文件结构不能更改，删除其中的文件会报错<br>


注意：<br>
1、生成代码的时候会直接覆盖<br>
2、maven 生成的jar包 会存在乱码问题：解决方式：命令行中执行  java -Dfile.encoding=utf-8 -jar APA_BUILD-1.0.0.jar<br>

 
 

1、生成代码的时候会直接覆盖<br>
2、必须设计表的时候<br>
	字段填写注释<br>
	表上填写注释<br>
3、数据库中设计必须有：id(自增),code(uuid,标识),create_time,create_code,update_code,update_time,del_flag(逻辑删除，1)<br>



编辑项目源码时卡死解决方案：
找到.project，用记事本打开，把两处删除掉：
第一处：
org.eclipse.wst.jsdt.core.javascriptValidator
第二处：
org.eclipse.wst.jsdt.core.jsNature
保存退出，refresh一下工程。会弹出一个提示框，选中复选框，点击否


