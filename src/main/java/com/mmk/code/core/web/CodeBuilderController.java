package com.mmk.code.core.web;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.mmk.code.common.SessionUtils;
import com.mmk.code.core.condition.BuildData;
import com.mmk.code.core.model.Field;
import com.mmk.code.core.model.Model;
import com.mmk.code.core.model.Project;
import com.mmk.code.core.model.ProjectAuthor;
import com.mmk.code.core.service.CodeBuilderService;
import com.mmk.code.core.service.FieldService;
import com.mmk.code.core.service.ModelService;
import com.mmk.code.core.service.ProjectAuthorService;
import com.mmk.code.core.service.ProjectService;

@RestController
public class CodeBuilderController {
	
	@Resource
	private CodeBuilderService builder;
	@Resource 
	private ProjectService projectService;
	@Resource 
	private ProjectAuthorService projectAuthorService;
	@Resource 
	private ModelService modelService;
	@Resource 
	private FieldService fieldService;

	@RequestMapping("/code/builde")
	@ResponseBody
	public String codeBuilder(@RequestBody BuildData data){
		
		String saveDate=saveData(data);
		if("保存成功2".equals(saveDate)){
			return "该表已锁定,执行解锁前请先保存";
		}
		String template = StringUtils.trimToEmpty(data.getProject().getTemplate());
		
		return builder.buildAllCode(template,data);
	}
	
	/**
	 * 
	 * @param data
	 * @return
	 */
	@RequestMapping("/code/save")
	@ResponseBody
	public String saveData(@RequestBody BuildData data){
		String result="保存成功";
		Project project = data.getProject();
		String path=project.getPath(); 
		project = projectService.save(project);
		project.setPath(path);
		ProjectAuthor projectAuthor = projectAuthorService.findByAuthorAndProjectId(SessionUtils.getSession().getAttribute("username").toString(), project.getId());
		data.setProjectAuthor(projectAuthor);		
		if(projectAuthor==null){
			projectAuthor =new ProjectAuthor();
		}
		projectAuthor.setAuthor(SessionUtils.getSession().getAttribute("username").toString());
		projectAuthor.setPath(path);
		projectAuthor.setProjectId(project.getId());
		projectAuthorService.save(projectAuthor);
		
		
		Model model = data.getModel();
		model.setProjectId(project.getId());
		model.setFlag(1);
		model.setAuthorFlag(SessionUtils.getSession().getAttribute("username").toString());
		//判断是否保存过
		if(model.getTableName()!=null){
			Model model1=modelService.findBy("tableName", model.getTableName());
			if(model1!=null){
				model.setId(model1.getId());
				if(model1.getFlag()!=null&&model1.getFlag()==1){
					result= "保存成功2";
				}
			}
			
		}
		
		if(!"".equals(model.getModel())){
			model = modelService.save(model);
		}
		List<Field> fieldList = data.getFieldList();
		for (Field field : fieldList) {
			//判断是否保存过
			Field fieldold=fieldService.findByModelIdAndColumn(model.getId(), field.getColumnName());
			if(fieldold!=null)
				field.setId(fieldold.getId());
		
			field.setModelId(model.getId());
			fieldService.save(field);
		}
		return result;
	}
	/**
	 * 
	 * @param data
	 * @return
	 */
	@RequestMapping("/code/lock")
	@ResponseBody
	public String saveLock(@RequestBody BuildData data){
		Project project = projectService.save(data.getProject());
		Model model = data.getModel();
		model.setProjectId(project.getId());
		model.setFlag(1);
		model.setAuthorFlag(model.getAuthor());
		if(model.getModel()==null||"".equals(model.getModel())){
			return "请选择要加/解锁的表";
		}
		
		
		//判断是否保存过
		if(model.getTableName()!=null){
			Model model1=modelService.findBy("tableName", model.getTableName());
			if(model1!=null){
				model.setId(model1.getId());
				if(model1.getFlag()!=null&&model1.getFlag()==1){
					return "该表已锁定";
				}
			}
			
		}
		
		if(!"".equals(model.getModel())){
			model = modelService.save(model);
		}
		List<Field> fieldList = data.getFieldList();
		for (Field field : fieldList) {
			//判断是否保存过
			Field fieldold=fieldService.findByModelIdAndColumn(model.getId(), field.getColumnName());
			if(fieldold!=null)
				field.setId(fieldold.getId());
		
			field.setModelId(model.getId());
			fieldService.save(field);
		}
		return "锁定成功";
	}
	
	/**
	 * 
	 * @param data
	 * @return
	 */
	@RequestMapping("/code/clear")
	@ResponseBody
	public String clearLock(@RequestBody BuildData data){
		Project project = projectService.save(data.getProject());
		Model model = data.getModel();
		model.setProjectId(project.getId());
		model.setFlag(0);
		model.setAuthorFlag("");
		//判断是否保存过
		if(model.getTableName()!=null){
			Model model1=modelService.findBy("tableName", model.getTableName());
			if(model1!=null){
				model.setId(model1.getId());
				if(!model.getAuthor().equals(model1.getAuthorFlag())){
					return "该表已被"+model1.getAuthorFlag()+"锁定,请联系作者解锁";
				}
			}
			
		} 
		
		if(!"".equals(model.getModel())){
			model = modelService.save(model);
		}
		List<Field> fieldList = data.getFieldList();
		for (Field field : fieldList) {
			//判断是否保存过
			Field fieldold=fieldService.findByModelIdAndColumn(model.getId(), field.getColumnName());
			if(fieldold!=null)
				field.setId(fieldold.getId());
		
			field.setModelId(model.getId());
			fieldService.save(field);
		}
		return "解锁成功";
	}
	
	@RequestMapping("/code/preview")
	public Map<String,Object> codePreview(@RequestBody BuildData data){
		
		String template = StringUtils.trimToEmpty(data.getProject().getTemplate());
		
		
		String modelcode = builder.buildModel(template,data);
		String condition = builder.buildCondition(template,data);
		String repository = builder.buildRepository(template,data);
		String dao = builder.buildDao(template,data);
		String daoImpl = builder.buildDaoImpl(template,data);
		String service = builder.buildService(template,data);
		String serviceImpl = builder.buildServiceImpl(template,data);
		String controller = builder.buildController(template,data);
		
		//页面部分生成
		
		String list = builder.buildList(template,data);
		String form = builder.buildForm(template,data);
		 
		String details = builder.buildDetails(template,data);
		
		//JS部分
		String listjs = builder.buildListJs(template,data);
		String formjs = builder.buildFormJs(template,data);
		String detailsjs = builder.buildDetailsJs(template,data);
		
		HttpSession session = SessionUtils.getSession();
		
		session.setAttribute("model", StringEscapeUtils.escapeHtml(modelcode));
		session.setAttribute("condition", StringEscapeUtils.escapeHtml(condition));
		session.setAttribute("repository", StringEscapeUtils.escapeHtml(repository));
		session.setAttribute("dao", StringEscapeUtils.escapeHtml(dao));
		session.setAttribute("daoImpl", StringEscapeUtils.escapeHtml(daoImpl));
		session.setAttribute("service", StringEscapeUtils.escapeHtml(service));
		session.setAttribute("serviceImpl", StringEscapeUtils.escapeHtml(serviceImpl));
		session.setAttribute("controller", StringEscapeUtils.escapeHtml(controller));
		//页面部分
		session.setAttribute("list", StringEscapeUtils.escapeHtml(list));
		session.setAttribute("form", StringEscapeUtils.escapeHtml(form)); 
		session.setAttribute("details", StringEscapeUtils.escapeHtml(details));
		
		//js部分预览代码
		session.setAttribute("listjs",StringEscapeUtils.escapeHtml(listjs));
		session.setAttribute("formjs",StringEscapeUtils.escapeHtml(formjs));
		session.setAttribute("detailsjs",StringEscapeUtils.escapeHtml(detailsjs));
		
		Map<String, Object> result = new HashMap<String,Object>();
		result.put("success", true);
		return result  ;
	}
	
	@RequestMapping("/code/helpview")
	public Map<String,Object> helpview(){
		
		 
		Map<String, Object> result = new HashMap<String,Object>();
		result.put("success", true);
		return result  ;
	}
	
	@RequestMapping("/code/helpview1")
	public ModelAndView helpPreview1(){
		ModelAndView view = new ModelAndView("helpview1"); 
		return view ;
	}
	@RequestMapping("/code/helpview2")
	public ModelAndView helpPreview2(){
		ModelAndView view = new ModelAndView("helpview2"); 
		return view ;
	}
	@RequestMapping("/code/showdir")
	public ModelAndView showdir(){
		ModelAndView view = new ModelAndView("showdir"); 
		return view ;
	}
	@RequestMapping("/code/preview/{code}")
	public ModelAndView codePreviewCode(@PathVariable String code){
		ModelAndView view = new ModelAndView("preview");
		view.addObject("code",SessionUtils.getSession().getAttribute(code));
		return view ;
	}
	
	@RequestMapping("/code/previewHtml/{code}")
	public ModelAndView codePreviewHtml(@PathVariable String code){
		ModelAndView view = new ModelAndView("previewhtml");
		view.addObject("code",SessionUtils.getSession().getAttribute(code));
		return view ;
	}
	
	
	
}
