package com.mmk.code.core.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Column;
import javax.persistence.Transient;

/**
*@Title: Project
*@Description: 项目 数据领域模型
*@author code generator
*@version 1.0
*@date 2016-07-19 14:16:55
*/
@Entity
@Table(name = "project")
public class Project {
    /**
     * ID
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="id")
    private Long id;

    /**
     * 项目名称
     */
    @Column(name="project_name")
    private String projectName;

    /**
     * 注释
     */
    @Column(name="comment")
    private String comment;

    /**
     * 对应数据库
     */
    @Column(name="database_name")
    private String databaseName;
    
    /**
     * 要使用的代码模板
     */
    @Column(name="template")
    private String template;

    /**
     * 目录地址
     */
    @Transient
    private String path;

    /**
     * 组织包
     */
    @Column(name="group_package")
    private String groupPackage;
    
    
    /**
     * 模块包
     */
    @Column(name="module_package")
    private String modulePackage;

    /**
     * 项目描述
     */
    @Column(name="description")
    private String description;
    
    /**
     * 0默认输出路径，1自定义输出路径
     */
    @Column(name="path_flag")
    private Integer pathFlag;
    /**
     * condition_java输出路径
     */
    @Column(name="condition_java")
    private String conditionJava;
    /**
     * controller_java输出路径
     */
    @Column(name="controller_java")
    private String controllerJava;
    /**
     * dao_java输出路径
     */
    @Column(name="dao_java")
    private String daoJava;
    /**
     * daoimpl_java输出路径
     */
    @Column(name="daoimpl_java")
    private String daoimplJava;
    /**
     * model_java输出路径
     */
    @Column(name="model_java")
    private String modelJava;
    /**
     * repository_java项目描述
     */
    @Column(name="repository_java")
    private String repositoryJava;
    /**
     * service_java
     */
    @Column(name="service_java")
    private String serviceJava;
    /**
     * serviceimpl_java
     */
    @Column(name="serviceimpl_java")
    private String serviceimplJava;
    /**
     * edit_js
     */
    @Column(name="edit_js")
    private String editJs;
    /**
     * details_js
     */
    @Column(name="details_js")
    private String detailsJs;
    /**
     * form_js
     */
    @Column(name="form_js")
    private String formJs;
    /**
     * list_js
     */
    @Column(name="list_js")
    private String listJs;
    /**
     * edit_jsp
     */
    @Column(name="edit_jsp")
    private String editJsp;
    /**
     * details_jsp
     */
    @Column(name="details_jsp")
    private String detailsJsp;
    /**
     * form_jsp
     */
    @Column(name="form_jsp")
    private String formJsp;
    /**
     * list_jsp
     */
    @Column(name="list_jsp")
    private String listJsp;
    


    /** 
	* @return id ：ID
	*/
    public Long getId() {
        return id;
    }
    /** 
    *@param id 设置ID 
    */
    public void setId(Long id) {
        this.id = id;
    }

    /** 
	* @return projectName ：项目名称
	*/
    public String getProjectName() {
        return projectName;
    }
    /** 
    *@param projectName 设置项目名称 
    */
    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    /** 
	* @return comment ：注释
	*/
    public String getComment() {
        return comment;
    }
    /** 
    *@param comment 设置注释 
    */
    public void setComment(String comment) {
        this.comment = comment;
    }

    /** 
	* @return databaseName ：对应数据库
	*/
    public String getDatabaseName() {
        return databaseName;
    }
    /** 
    *@param databaseName 设置对应数据库 
    */
    public void setDatabaseName(String databaseName) {
        this.databaseName = databaseName;
    }

    /** 
	* @return path ：目录地址
	*/
    public String getPath() {
        return path;
    }
    /** 
    *@param path 设置目录地址 
    */
    public void setPath(String path) {
        this.path = path;
    }

    /** 
	* @return groupPackage ：组织包
	*/
    public String getGroupPackage() {
        return groupPackage;
    }
    /** 
    *@param groupPackage 设置组织包 
    */
    public void setGroupPackage(String groupPackage) {
        this.groupPackage = groupPackage;
    }

    /** 
	* @return description ：项目描述
	*/
    public String getDescription() {
        return description;
    }
    /** 
    *@param description 设置项目描述 
    */
    public void setDescription(String description) {
        this.description = description;
    }
	public String getTemplate() {
		return template;
	}
	public void setTemplate(String template) {
		this.template = template;
	}
	public String getModulePackage() {
		return modulePackage;
	}
	public void setModulePackage(String modulePackage) {
		this.modulePackage = modulePackage;
	}
	 
	public String getConditionJava() {
		return conditionJava;
	}
	public void setConditionJava(String conditionJava) {
		this.conditionJava = conditionJava;
	}
	public String getControllerJava() {
		return controllerJava;
	}
	public void setControllerJava(String controllerJava) {
		this.controllerJava = controllerJava;
	}
	public String getDaoJava() {
		return daoJava;
	}
	public void setDaoJava(String daoJava) {
		this.daoJava = daoJava;
	}
	public String getDaoimplJava() {
		return daoimplJava;
	}
	public void setDaoimplJava(String daoimplJava) {
		this.daoimplJava = daoimplJava;
	}
	public String getModelJava() {
		return modelJava;
	}
	public void setModelJava(String modelJava) {
		this.modelJava = modelJava;
	}
	public String getRepositoryJava() {
		return repositoryJava;
	}
	public void setRepositoryJava(String repositoryJava) {
		this.repositoryJava = repositoryJava;
	}
	public String getServiceJava() {
		return serviceJava;
	}
	public void setServiceJava(String serviceJava) {
		this.serviceJava = serviceJava;
	}
	public String getServiceimplJava() {
		return serviceimplJava;
	}
	public void setServiceimplJava(String serviceimplJava) {
		this.serviceimplJava = serviceimplJava;
	}
	public String getDetailsJs() {
		return detailsJs;
	}
	public void setDetailsJs(String detailsJs) {
		this.detailsJs = detailsJs;
	}
	public String getFormJs() {
		return formJs;
	}
	public void setFormJs(String formJs) {
		this.formJs = formJs;
	}
	public String getListJs() {
		return listJs;
	}
	public void setListJs(String listJs) {
		this.listJs = listJs;
	}
	public String getDetailsJsp() {
		return detailsJsp;
	}
	public void setDetailsJsp(String detailsJsp) {
		this.detailsJsp = detailsJsp;
	}
	public String getFormJsp() {
		return formJsp;
	}
	public void setFormJsp(String formJsp) {
		this.formJsp = formJsp;
	}
	public String getListJsp() {
		return listJsp;
	}
	public void setListJsp(String listJsp) {
		this.listJsp = listJsp;
	}
	public Integer getPathFlag() {
		return pathFlag;
	}
	public void setPathFlag(Integer pathFlag) {
		this.pathFlag = pathFlag;
	}
	public String getEditJs() {
		return editJs;
	}
	public void setEditJs(String editJs) {
		this.editJs = editJs;
	}
	public String getEditJsp() {
		return editJsp;
	}
	public void setEditJsp(String editJsp) {
		this.editJsp = editJsp;
	}
	
	
	
}